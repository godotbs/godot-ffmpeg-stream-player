#!/usr/bin/env python
import re
import os
import sys
import glob

# The name of the module
name = "godot-ffmpeg-stream-player"

# Libraries for windows
win_libs = ['pdh', 'psapi','nvml']

# Add specific variables
# Godot version target
godot_version = ARGUMENTS.get("godot_version", "4.1")
if "godot_version" in ARGUMENTS:
    del ARGUMENTS["godot_version"]

# Sanitize godot_version
if not re.match(r"^\d+[.]\d+$", godot_version):
    print("Error: specified `godot_version` must be in the form major.minor such as '4.1'.")
    Exit(1)

godot_target_major, godot_target_minor = godot_version.split('.')

# Ensure godot-cpp repo is cloned at all
os.system(f"git submodule update --init")
os.system(f"cd godot-cpp; git fetch")

# Use that version of godot
godot_cpp_path = os.path.join(os.path.expanduser('~'), '.cache', f"godot-cpp-{godot_version}")
if not os.path.exists(godot_cpp_path):
    os.system(f"git clone godot-cpp {godot_cpp_path} --shared")
if not os.path.exists(f"godot-cpp-{godot_version}"):
    os.symlink(godot_cpp_path, f"godot-cpp-{godot_version}", target_is_directory=True)
os.system(f"cd {godot_cpp_path}; git checkout godot-{godot_version}-stable")

# Import godot-cpp scons
env = SConscript(f"{godot_cpp_path}/SConstruct")

# Organize a set of flags
flags = []

# The targets we are building
targets = []

# For reference:
# - CCFLAGS are compilation flags shared between C and C++
# - CFLAGS are for C-specific compilation flags
# - CXXFLAGS are for C++-specific compilation flags
# - CPPFLAGS are for pre-processor flags
# - CPPDEFINES are for pre-processor defines
# - LINKFLAGS are for linking flags

# tweak this if you want to use different folders, or more folders, to store your source code in.
flags.append(f"-Isrc")
sources = Glob("src/*.cpp")

# Add ffmpeg include path
flags.append(f"-Ithird-party/{env['platform']}-{env['arch']}/include")

# Negotiate bitness
env.Append(TARGET_ARCH='i386' if env['platform'].endswith('32') else 'x86_64')

# Add ffmpeg link options
output_path = '#bin/' + env['platform'] + '/'

# Determine where our dependencies are built and installed to
lib_prefix = os.path.join('third-party', f"{env['platform']}-{env['arch']}")
lib_path = lib_prefix + '/lib'

if not os.path.exists(f"FFmpeg-{env['platform']}-{env['arch']}"):
  os.system(f"git clone FFmpeg FFmpeg-{env['platform']}-{env['arch']} --shared")

# Build our dependencies
check_path = os.path.join(lib_path, 'libavcodec.so')
if env['platform'] == 'windows':
    check_path = os.path.join(lib_path, 'libavcodec.dll.a')

if not os.path.exists(check_path):
    if env['platform'] == 'windows':
        # Build FFmpeg for windows
        os.system(f"cd FFmpeg-{env['platform']}-{env['arch']}; ./configure --target-os=mingw64 --arch=x86_64 --windres=x86_64-w64-mingw32-windres --cc=x86_64-w64-mingw32-gcc --cxx=x86_64-w64-mingw32-g++ --disable-doc --disable-htmlpages --disable-manpages --disable-podpages --disable-txtpages --enable-shared --extra-ldflags='-Wl,-Bstatic -lpthread' --prefix=$PWD/../third-party/{env['platform']}-{env['arch']} --cross-prefix=x86_64-w64-mingw32-")
    else:
        # Build FFmpeg for linux
        os.system(f"cd FFmpeg-{env['platform']}-{env['arch']}; ./configure --enable-shared --disable-doc --disable-htmlpages --disable-manpages --disable-podpages --disable-txtpages --prefix=$PWD/../third-party/{env['platform']}-{env['arch']}")

    os.system(f"cd FFmpeg-{env['platform']}-{env['arch']}; make")
    os.system(f"cd FFmpeg-{env['platform']}-{env['arch']}; make install")
    if not os.path.exists(check_path):
        print("ERROR: Could not find a successful FFmpeg build.")
        Exit(1)
else:
    print("NOTE: Found an existing FFmpeg build.")
    print(f"      Delete the third-party/{env['platform']}-{env['arch']} path to rebuild.")

# Add FFmpeg libraries via pkgconfig
for libname in glob.glob(os.path.join(lib_path, '*.a')):
    libname = os.path.splitext(os.path.basename(libname))[0]
    if '.dll' in libname:
        libname = os.path.splitext(os.path.basename(libname))[0]
    env.ParseConfig(f"PKG_CONFIG_PATH={lib_path}/pkgconfig pkg-config {libname} --libs")

if env['platform'] == 'windows':
    # Windows needs to link to the dlls
    lib_path = os.path.join(lib_path, '..', 'bin')

# Add FFmpeg lib path
flags.append(f"-L{lib_path}")

# Add our own lib_path
flags.append(f"-L{output_path}")

# Create gdextension file from template
gdextension = env.Command(target=f'dist/{godot_version}/{name}/{name}.gdextension',
                          source='./module.gdextension',
                          action="sed -e 's/gdexample/" + name + f"/g' -e 's/x[.]x/{godot_version}/g' < $SOURCE > $TARGET")

# Add any extra files
if env["platform"] == "windows":
    for libname in glob.glob(os.path.join(lib_path, '*.dll')):
        libname = os.path.basename(libname)
        targets.append(
            env.Command(
                f'dist/{godot_version}/{name}/{env["platform"]}/{env["arch"]}/{libname}',
                f'./third-party/{env["platform"]}-{env["arch"]}/bin/{libname}',
                Copy('$TARGET', '$SOURCE')
            )
        )
else:
    for libname in glob.glob(os.path.join(lib_path, '*.so*')):
        libname = os.path.basename(libname)
        targets.append(
            env.Command(
                f'dist/{godot_version}/{name}/{env["platform"]}/{env["arch"]}/{libname}',
                f'./third-party/{env["platform"]}-{env["arch"]}/lib/{libname}',
                Copy('$TARGET', '$SOURCE')
            )
        )

# Determine the library name for our output
if env["platform"] == "macos":
    library = env.SharedLibrary(
        "dist/{}/{}/osx/bin/lib{}.{}.{}.framework/lib{}.{}.{}".format(
            godot_version, name,
            name, env["platform"], env["target"],
            name, env["platform"], env["target"]
        ),
        source=sources,
        parse_flags=' '.join(flags),
    )
else:
    library = env.SharedLibrary(
        "dist/{}/{}/{}/{}/lib{}{}{}".format(
            godot_version,
            name,
            env["platform"],
            env["arch"],
            name, env["suffix"], env["SHLIBSUFFIX"]
        ),
        source=sources,
        parse_flags=' '.join(flags),
    )

# Tar/Zip distributions
targets.append(
    env.Zip(f"dist/{name}_godot-{godot_version}", [f"dist/{godot_version}"], ZIPROOT=f"dist/{godot_version}")
)

Default(library, gdextension, *targets)
