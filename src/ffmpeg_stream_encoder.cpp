#include "ffmpeg_stream_encoder.hpp"

#include <godot_cpp/variant/utility_functions.hpp>

using namespace godot;

/**
 * Establish methods we expose to GDScript and the Godot UI.
 */
void FFMpegStreamEncoder::_bind_methods() {
	// target property
	ClassDB::bind_method(D_METHOD("get_target"), &FFMpegStreamEncoder::get_target);
	ClassDB::bind_method(D_METHOD("set_target", "path"), &FFMpegStreamEncoder::set_target);
	ClassDB::add_property("FFMpegStreamEncoder", PropertyInfo(Variant::STRING, "target"), "set_target", "get_target");

	// texture property
	ClassDB::bind_method(D_METHOD("get_texture"), &FFMpegStreamEncoder::get_texture);
	ClassDB::bind_method(D_METHOD("set_texture", "texture"), &FFMpegStreamEncoder::set_texture);
	ClassDB::add_property("FFMpegStreamEncoder", PropertyInfo(Variant::OBJECT, "texture", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_texture", "get_texture");

	// General methods
	ClassDB::bind_method(D_METHOD("record"), &FFMpegStreamEncoder::record);
	ClassDB::bind_method(D_METHOD("pause"), &FFMpegStreamEncoder::pause);
	ClassDB::bind_method(D_METHOD("resume"), &FFMpegStreamEncoder::resume);
	ClassDB::bind_method(D_METHOD("stop"), &FFMpegStreamEncoder::stop);
	ClassDB::bind_method(D_METHOD("write_frame"), &FFMpegStreamEncoder::write_frame);
}

/**
 * Construct and initialize the encoder.
 */
FFMpegStreamEncoder::FFMpegStreamEncoder() {
	// Ensure things are in the 'unprepared' state.
	this->_unprepare_encoder();
}

/**
 * Clean up.
 */
FFMpegStreamEncoder::~FFMpegStreamEncoder() {
	// Stop
	this->stop();

	// Destruct our encoder contexts
	this->_unprepare_encoder();

	// Stop our thread
	if (this->_worker.joinable()) {
		this->_recording = false;
		this->_worker.join();
	}
}

/**
 * Initializes structures and such for encoding.
 */
bool FFMpegStreamEncoder::_prepare_encoder() {
	// Do not allow prepare to be called twice
	UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: start");
	if (this->_preparing) {
		return false;
	}
	this->_preparing = true;

	// Clean up anything left over, if necessary.
	this->_unprepare_encoder();

	// Allocate a format context based on the type of output
	std::string path = this->_target.utf8().get_data();
	std::string filename = path.substr(path.find_last_of("/\\") + 1);
	std::string scheme = "file";

	if (path.find(':') != std::string::npos) {
		scheme = path.substr(0, path.find(':'));
	}
	UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: target scheme: ", scheme.c_str());

	AVDictionary* io_opts = NULL;

	if (scheme == "rtmp") {
		// Live-stream output via RTMP... get a format context for the stream
		avformat_alloc_output_context2(&this->_format_ctx, NULL, "flv", path.c_str());

		// Set RTMP settings
		av_dict_set(&io_opts, "rtmp_live", "live", 0); // Set live streaming mode
		av_dict_set(&io_opts, "stimeout", "2000000", 0); // Set a timeout of 2 seconds
	}
	else {
		// Just create a blank one
		this->_format_ctx = avformat_alloc_context();
	}

	// Check to see that we now have a valid format context
	if (this->_format_ctx == NULL) {
		// Error (Could not create format context)
		UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: error: cannot allocate format context");
		this->_unprepare_encoder();
		return false;
	}

	// If this is an output file, we can negotiate the output format
	if (scheme != "rtmp") {
		// Get the output format we need.
		const AVOutputFormat* output_fmt = av_guess_format(NULL, filename.c_str(), NULL);
		this->_format_ctx->oformat = output_fmt;
	}

	// Check to see that we now have a valid output format
	if (this->_format_ctx->oformat == NULL) {
		// Error (Could not query output format)
		UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: error: cannot query output format");
		this->_unprepare_encoder();
		return false;
	}

	// Determine width and height
	int width = 1920;
	int height = 1080;

	// Create a stream
	this->_stream = avformat_new_stream(this->_format_ctx, NULL);
	if (this->_stream == NULL) {
		// Error (Could not create stream)
		UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: error: cannot allocate stream");
		this->_unprepare_encoder();
		return false;
	}

	// Choose an appropriate codec
	if (scheme == "rtmp") {
		this->_stream->codecpar->codec_id = AV_CODEC_ID_H264;
	}
	else if (strcmp(this->_format_ctx->oformat->name, "mp4") == 0 || strcmp(this->_format_ctx->oformat->name, "matroska") == 0) {
		this->_stream->codecpar->codec_id = AV_CODEC_ID_H264;
	} else if (strcmp(this->_format_ctx->oformat->name, "avi") == 0) {
		this->_stream->codecpar->codec_id = AV_CODEC_ID_MPEG4;
	} else {
		// Error (Could not determine codec)
		UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: error: cannot determine codec");
		this->_unprepare_encoder();
		return false;
	}

	// Set other stream properties
	this->_stream->codecpar->codec_id = AV_CODEC_ID_H264;
	this->_stream->codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
	this->_stream->codecpar->width = width;
	this->_stream->codecpar->height = height;
	this->_stream->codecpar->format = AV_PIX_FMT_YUV420P; 
	this->_stream->time_base = (AVRational){1, 60};
	this->_stream->r_frame_rate = (AVRational){60, 1};

	// Acquire a video codec
	const AVCodec* vcodec = avcodec_find_encoder(this->_stream->codecpar->codec_id);
	if (vcodec == NULL) {
		// Error (Could not find video encoder)
		UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: error: cannot find video encoder");
		this->_unprepare_encoder();
		return false;
	}

	// Allocate video codec context
	this->_vcodec_ctx = avcodec_alloc_context3(vcodec);
	if (this->_vcodec_ctx == NULL) {
		// Error (Could not allocate video codec)
		UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: error: cannot allocate video codec");
		this->_unprepare_encoder();
		return false;
	}

	if (avcodec_parameters_to_context(this->_vcodec_ctx, this->_stream->codecpar) < 0) {
		// Error (Could not initialize the video codec context)
		UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: error: cannot initialize video codec context");
		this->_unprepare_encoder();
		return false;
	}

	// Set other parameters
	// Enable multi-thread encoding based on CPU core count.
	this->_vcodec_ctx->thread_count = 0;
	this->_vcodec_ctx->gop_size = 120;
	this->_vcodec_ctx->bit_rate = 8000000; // 8 Mbps
	this->_vcodec_ctx->max_b_frames = 2;
	this->_vcodec_ctx->time_base = this->_stream->time_base;
	this->_vcodec_ctx->framerate = this->_stream->r_frame_rate;

	// Open the video codec
	AVDictionary *opts = NULL;
	av_dict_set(&opts, "preset", "medium", 0);    // use the "medium" preset
	av_dict_set(&opts, "crf", "23", 0);           // set CRF value
	int result = avcodec_open2(this->_vcodec_ctx, vcodec, &opts);
	if (result < 0) {
		// Error (Could not open video codec)
		UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: error: cannot open video codec");
		UtilityFunctions::print(av_err2string(result).c_str());
		this->_unprepare_encoder();
		return false;
	}
	this->_vcodec_open = true;

	// Open output file
	UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: output open");
	avio_open2(&this->_format_ctx->pb, path.c_str(), AVIO_FLAG_WRITE, NULL, &io_opts);
	UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: output write header");
	if (avformat_write_header(this->_format_ctx, NULL) < 0) {
		// Error (Could not write to the output)
		UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: error: cannot write to output");
		this->_unprepare_encoder();
		return false;
	}

	// Note: The codec might force time_base values!
	// We should not assume the ones we set are the final result.

	// Get a Sws context to convert color space
	// TODO: what is the right scale function? BICUBIC?
	this->_sws_ctx = sws_getContext(
		width,
		height,
		AV_PIX_FMT_RGBA,
		width,
		height,
		AV_PIX_FMT_YUV420P,
		SWS_BICUBIC,
		NULL,
		NULL,
		NULL
	);
	if (this->_sws_ctx == NULL) {
		// Error (Could not create the Swscale context)
		UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: error: cannot create scaler context");
		this->_unprepare_encoder();
		return false;
	}

	// Initialize counters and things
	this->_frame_position = 0;

	// Done.
	UtilityFunctions::print("FFMpegStreamEncoder::_prepare_encoder: ready");
	this->_preparing = false;
	return true;
}

/**
 * Deinitializes and cleans up structures used for encoding.
 */
bool FFMpegStreamEncoder::_unprepare_encoder() {
	UtilityFunctions::print("FFMpegStreamEncoder::_unprepare_encoder: start");

	// Close and free FFmpeg video codec context
	if (this->_vcodec_ctx) {
		if (this->_vcodec_open) {
			avcodec_close(this->_vcodec_ctx);
		}
		avcodec_free_context(&this->_vcodec_ctx);
		this->_vcodec_ctx = NULL;
	}

	// Free FFmpeg AVFormatContext
	if (this->_format_ctx) {
		avformat_free_context(this->_format_ctx);
		this->_format_ctx = NULL;
	}

	UtilityFunctions::print("FFMpegStreamEncoder::_unprepare_encoder: end");
	return true;
}

/**
 * This loop continuously produces a video frame.
 */
void FFMpegStreamEncoder::_loop() {
	UtilityFunctions::print("FFMpegStreamEncoder::_loop starting");

	// Process each frame
	while (this->_recording) {
		// this->_write_frame();
	}

	UtilityFunctions::print("FFMpegStreamEncoder::_loop ending");
}

/**
 * This returns a reference to our byte buffer of video data.
 */
uint8_t* FFMpegStreamEncoder::_fill_frame_buffer() {
	if (this->_texture != NULL) {
		// Get the image
		Ref<Image> image = this->_texture->get_image();
		this->_frame_data = image->get_data();
		this->_buffer = this->_frame_data.ptrw();
	}

	return this->_buffer;
}

/**
 * This writes one frame to the stream.
 */
bool FFMpegStreamEncoder::_write_frame() {
	// Get the frame data from the texture
	uint8_t* buffer = this->_fill_frame_buffer();
	int width = 1920;
	int height = 1080;

	AVFrame* frameRGBA = av_frame_alloc();
	frameRGBA->format = AV_PIX_FMT_RGBA;
	frameRGBA->width = width;
	frameRGBA->height = height;

	// We can get a buffer from FFmpeg that it likes...
	if (0) {
		// Copy the data from our buffer to the FFmpeg one.
		av_frame_get_buffer(frameRGBA, 0);
		memcpy(frameRGBA->data[0], buffer, width * height * 4);
	}
	else {
		// Wrap our buffer in the frame data
		frameRGBA->linesize[0] = width * 4;
		frameRGBA->data[0] = buffer;

		// Ensure that it knows we are managing the buffer
		frameRGBA->buf[0] = NULL;
	}

	AVFrame* frameYUV = av_frame_alloc();
	frameYUV->format = AV_PIX_FMT_YUV420P;
	frameYUV->width = width;
	frameYUV->height = height;
	av_frame_get_buffer(frameYUV, 0);

	// Convert RGBA buffer to YUV420P
	sws_scale(
		this->_sws_ctx,
		frameRGBA->data, frameRGBA->linesize, 0, height,
		frameYUV->data, frameYUV->linesize
	);

	// What frame number is this? record the timestamp relative
	// to the frame rate.
	AVRational time_base = this->_vcodec_ctx->time_base;
	frameYUV->pts = this->_frame_position * time_base.den / 60.0; //this->_fps;
	this->_frame_position++;

	bool ret = false;

	// Send the frame to the encoder... it will, in repsonse, send packets to us
	// that are encoded. We will then send those to the output as they come in.
	int result = avcodec_send_frame(this->_vcodec_ctx, frameYUV);
	if (result == AVERROR_EOF) {
		UtilityFunctions::print("FFMpegStreamEncoder::_write_frame: result is eof");
		ret = true;
	}
	else if (result < 0) {
		UtilityFunctions::print("FFMpegStreamEncoder::_write_frame: result is error");
		ret = false;
	}
	else {
		// Form packet
		AVPacket* pkt = av_packet_alloc();
		if (pkt != NULL) {
			// Loop through the available packets
			while(avcodec_receive_packet(this->_vcodec_ctx, pkt) == 0) {
				// Correct the packet's stream index for muxing
				pkt->stream_index = this->_format_ctx->streams[0]->index;

				// We need to compute the pts according to the stream's time_base
				pkt->pts = av_rescale_q(frameYUV->pts, this->_vcodec_ctx->time_base, this->_stream->time_base);
				pkt->dts = pkt->pts;

				// Write the packet to the output
				result = av_interleaved_write_frame(this->_format_ctx, pkt);
				UtilityFunctions::print("FFMpegStreamEncoder::_write_frame: interleaved write (", this->_frame_position - 1, "): ", result, " pts: ", frameYUV->pts);
				if (result < 0) {
					UtilityFunctions::print(av_err2string(result).c_str());
				}

				// Unref the packet so we can use it again.
				av_packet_unref(pkt);
			}

			// Free the packet
			av_packet_free(&pkt);
			ret = true;
		}
		else {
			ret = false;
		}
	}

	av_frame_free(&frameRGBA);
	av_frame_free(&frameYUV);
	UtilityFunctions::print("FFMpegStreamEncoder::_write_frame: done");

	return ret;
}

/**
 * Starts to record the texture to the given target.
 */
void FFMpegStreamEncoder::record() {
	if (this->_paused) {
		this->resume();
		return;
	}

	// Stop ourselves if we were already recording
	this->stop();

	// Ensure that the state is prepared
	this->_prepare_encoder();

	// Start producer thread
	this->_worker = std::thread(&FFMpegStreamEncoder::_loop, this);
}

/**
 * Pauses encoding.
 */
void FFMpegStreamEncoder::pause() {
	// Just tell the producing thread to not produce frames
	this->_paused = true;
}

/**
 * Resumes encoding.
 */
void FFMpegStreamEncoder::resume() {
	// Just tell the producing thread to now produce frames
	this->_paused = false;
}

/**
 * Stops encoding and finalizes the target.
 */
void FFMpegStreamEncoder::stop() {
	UtilityFunctions::print("FFMpegStreamEncoder::stop: start");

	// Stop the producing thread
	if (this->_worker.joinable()) {
		this->_recording = false;
		this->_worker.join();
	}

	UtilityFunctions::print("FFMpegStreamEncoder::stop: thread joined");

	// Finalize the output
	if (this->_format_ctx) {
		UtilityFunctions::print("FFMpegStreamEncoder::stop: flushing");

		// We will now flush the encoder
		AVPacket* pkt = av_packet_alloc();
		if (pkt != NULL) {
			// Send a NULL frame to indicate the end of the stream and flush the encoder
			avcodec_send_frame(this->_vcodec_ctx, NULL);

			// Now get all frames stuck in the pipe
			while (avcodec_receive_packet(this->_vcodec_ctx, pkt) == 0) {
				// Corrent for the stream index so it properly muxes
				pkt->stream_index = this->_format_ctx->streams[0]->index;

				// Write the flushed frame to the file
				av_interleaved_write_frame(this->_format_ctx, pkt);

				// Unref so we can use the packet again
				av_packet_unref(pkt);
			}
			UtilityFunctions::print("FFMpegStreamEncoder::stop: flushed");

			// Free the packet
			av_packet_free(&pkt);
		}

		// Write outgoing bytes for the output
		int result = av_write_trailer(this->_format_ctx);
		UtilityFunctions::print("FFMpegStreamEncoder::stop: writing trailer: ", result);

		// Close the file
		avio_closep(&this->_format_ctx->pb);
	}

	// Clean up
	this->_unprepare_encoder();
}

/**
 * Tells the engine that it should write another frame.
 */
void FFMpegStreamEncoder::write_frame(float time) {
	// Track the time and if we missed a frame

	// Compute the stream timestamp

	// Tell the producer thread to continue

	this->_write_frame();
}

/**
 * Sets the target for the encoding.
 */
void FFMpegStreamEncoder::set_target(String path) {
	if (this->_target != path) {
		// Retain the target
		this->_target = path;

		// Prepare the output encoding contexts
		this->_prepare_encoder();
	}
}

/**
 * Gets the previously set target for the encoding.
 */
String FFMpegStreamEncoder::get_target() const {
	return this->_target;
}

/**
 * Sets the texture to be used as the encoding source.
 */
void FFMpegStreamEncoder::set_texture(Ref<Texture2D> texture) {
	this->_texture = texture;
}

/**
 * Retrieves the previously set texture used as the encoding source.
 */
Ref<Texture2D> FFMpegStreamEncoder::get_texture() const {
	return this->_texture;
}
