#ifndef FFMPEG_STREAM_ENCODER_HPP
#define FFMPEG_STREAM_ENCODER_HPP

// Import standard library functions
#include <iostream>
#include <fstream>
#include <queue>
#include <thread>
#include <chrono>
#include <mutex>

// Import Godot support classes
#include <godot_cpp/core/class_db.hpp>

// Import base class
#include <godot_cpp/classes/object.hpp>

// Import support classes
#include <godot_cpp/classes/ref.hpp>
#include <godot_cpp/templates/vector.hpp>
#include <godot_cpp/classes/image.hpp>
#include <godot_cpp/classes/image_texture.hpp>

// Import FFMpeg
#include "ffmpeg_common.h"

namespace godot {
	/**
	 * This class wraps FFmpeg to allow the encoding of a video stream from a
	 * given 'texture' to the given 'target'.
	 *
	 * The 'target' can be a file path with the usual 'user://' or such URL
	 * schemes that target the file-system. Or it can be a stream URL that
	 * FFmpeg understands, such as an RTMP location.
	 */
	class FFMpegStreamEncoder : public Object {
		GDCLASS(FFMpegStreamEncoder, Object)

		private:
			// Holds the target of the encoding
			String _target;

			// Texture we are encoding
			Ref<Texture2D> _texture;

			// Capture loop worker
			std::thread _worker;
			std::mutex _time_mutex;

			// And consumer thread entrypoint
			void _loop();

			// Member properties
			bool _preparing = false;	// Preparing the encoder
			bool _recording = false;	// Whether we are recording
			bool _paused = false;		// Whether we suspend pushing frames out

			// FFmpeg context
			AVFormatContext* _format_ctx = NULL;
			AVStream* _stream = NULL;
			AVCodecContext* _vcodec_ctx = NULL;
			bool _vcodec_open = false;
			AVCodecContext* _acodec_ctx = NULL;
			bool _acodec_open = false;
			SwsContext* _sws_ctx = NULL;

			// Buffers
			PackedByteArray _frame_data;	// Reference wrapping the source texture data
			uint8_t* _buffer;		// Unreliable pointer to internal frame data

			// Frame position
			int _frame_position = 0;

		public:
			// Constructor / Destructor
			FFMpegStreamEncoder();
			~FFMpegStreamEncoder();

			// Perform recording
			void record();

			// Pause recording
			void pause();

			// Resume recording
			void resume();

			// Stop recording
			void stop();

			// Notify frame update
			void write_frame(float time);

			// target property
			String get_target() const;
			void set_target(String target);

			// texture property
			Ref<Texture2D> get_texture() const;
			void set_texture(Ref<Texture2D> target);

		protected:
			// Godot bind call
			static void _bind_methods();

			// Basic functions
			bool _prepare_encoder();
			bool _unprepare_encoder();

			// Fills the buffer used to write the video frame
			uint8_t* _fill_frame_buffer();

			// Frame writing
			bool _write_frame();
	};

}

#endif
