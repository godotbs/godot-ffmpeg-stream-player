#ifndef FFMPEG_STREAM_PLAYER_HPP
#define FFMPEG_STREAM_PLAYER_HPP

// Import standard library functions
#include <iostream>
#include <fstream>
#include <queue>
#include <thread>
#include <chrono>
#include <mutex>

// Import Godot support classes
#include <godot_cpp/core/class_db.hpp>

// Import base class
#include <godot_cpp/classes/sprite2d.hpp>

// Import support classes
#include <godot_cpp/classes/ref.hpp>
#include <godot_cpp/templates/vector.hpp>
#include <godot_cpp/classes/image.hpp>
#include <godot_cpp/classes/image_texture.hpp>

// Import FFMpeg
#include "ffmpeg_common.h"

namespace godot {

	class FFMpegStreamPlayer : public Sprite2D {
		GDCLASS(FFMpegStreamPlayer, Sprite2D)

		enum POSITION_TYPE {
			POS_V_PTS,
			POS_TIME,
			POS_A_TIME
		};

		private:
			bool _realtime = false;
			bool _preparing = false;
			bool _playing = false;
			double _time = 0.0;
			double _seek_time = 0.0;
			double _next_ts = -1;
			double _audio_time = -1.0;
			double _diff_tolerance = 0.0;
			mutable double _duration = -1.0;
			mutable int64_t _start_pts = -1;
			mutable int64_t _frames = -1;
			mutable int _fps_num = 1;
			mutable int _fps_den = 1;
			enum POSITION_TYPE _position_type;
			unsigned long _drop_frame = 0;
			unsigned long _total_frame = 0;
			int _num_decoded_samples = 0;
			bool _input_open = false;
			bool _vcodec_open = false;
			bool _acodec_open = false;
			int _videostream_idx = -1;
			int _audiostream_idx = -1;
			unsigned char* _io_buffer = NULL;
			String _source;
			float* _audio_buffer = NULL;
			int _audio_buffer_pos = 0;
			uint8_t* _frame_buffer = NULL;
			int _frame_buffer_size;
			AVFrame* _frame_yuv = NULL;
			AVFrame* _frame_rgb = NULL;
			PackedByteArray _frame_data;
			Ref<ImageTexture> _texture;
			bool _frame_unwrapped = false;
			AVFrame* _audio_frame = NULL;
			SwrContext* _swr_ctx = NULL;
			SwsContext* _sws_ctx = NULL;
			AVIOContext* _io_ctx = NULL;
			AVFormatContext* _format_ctx = NULL;
			AVCodecContext* _vcodec_ctx = NULL;
			AVCodecContext* _acodec_ctx = NULL;
			bool _loops = false;
			bool _eof = false;
			bool _stopped_eof = false;

			std::queue<AVPacket> _audio_packet_queue;
			std::queue<AVPacket> _video_packet_queue;

			mutable std::ifstream _file;
			std::streampos _file_size;

			static int _file_read(void* opaque, uint8_t* buf, int buf_size);
			static int64_t _file_seek(void* opaque, int64_t offset, int whence);

			int _read_frame();
			float get_framerate() const;
			float get_videoframe(float time);
			void _unwrap_video_frame(AVFrame* frame, int width, int height);

			int64_t _get_stream_start_position() const;
			int64_t _frame_to_pts(int frame) const;
			int _pts_to_frame(int64_t pts) const;

			static void _flush_frames(AVCodecContext* ctx);
			static double _avtime_to_sec(int64_t avtime);

			// Capture loop worker
			std::thread _worker;
			std::mutex _time_mutex;

			// And consumer thread entrypoint
			void _loop();

		public:
			FFMpegStreamPlayer();
			~FFMpegStreamPlayer();

			void _process(float delta);

			void set_audio_track(int audiotrack);
			String get_source() const;
			void set_source(String source);
			int get_channels() const;
			int get_mix_rate() const;
			float get_duration() const;
			int get_frames() const;
			float get_stream_position() const;
			void set_stream_position(float value);
			Vector2 get_size() const;
			Ref<Texture2D> get_video_texture() const;
			bool get_loops() const;
			void set_loops(bool value);
			int get_audio();

			void play();
			void pause();
			void stop();

		protected:
			virtual AVFormatContext* _prepare_format();
			virtual bool _is_real_time();
			virtual void _set_options(AVDictionary** format_opts) const;
			bool _prepare_decoder();
			bool _unprepare_decoder();
			static void _bind_methods();
	};

}

#endif
