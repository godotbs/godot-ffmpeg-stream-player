#include "ffmpeg_stream_player.hpp"

#include <godot_cpp/variant/utility_functions.hpp>

using namespace godot;

// File reading buffer of 512 KiB
const int IO_BUFFER_SIZE = 512 * 1024;

// The audio buffer max size (bytes)
const int AUDIO_BUFFER_MAX_SIZE = 192000;

// The video buffer (non real-time)
#define VIDEO_BUFFER_MAX 24

// TODO: is this sample rate defined somewhere in the godot api etc?
#define AUDIO_MIX_RATE 22050

// Clock Setup function (used by get_ticks_usec)
static uint64_t _clock_start = 0;
#if defined(__APPLE__)
static double _clock_scale = 0;
static void _setup_clock() {
	mach_timebase_info_data_t info;
	kern_return_t ret = mach_timebase_info(&info);
	_clock_scale = ((double)info.numer / (double)info.denom) / 1000.0;
	_clock_start = mach_absolute_time() * _clock_scale;
}
#elif defined(_MSC_VER)
uint64_t ticks_per_second;
uint64_t ticks_start;
static uint64_t get_ticks_usec();
static void _setup_clock() {
	// We need to know how often the clock is updated
	if (!QueryPerformanceFrequency((LARGE_INTEGER *)&ticks_per_second))
		ticks_per_second = 1000;
	ticks_start = 0;
	ticks_start = get_ticks_usec();
}
#else
#if defined(CLOCK_MONOTONIC_RAW) && !defined(JAVASCRIPT_ENABLED) // This is a better clock on Linux.
#define GODOT_CLOCK CLOCK_MONOTONIC_RAW
#else
#define GODOT_CLOCK CLOCK_MONOTONIC
#endif
static void _setup_clock() {
	struct timespec tv_now = { 0, 0 };
	clock_gettime(GODOT_CLOCK, &tv_now);
	_clock_start = ((uint64_t)tv_now.tv_nsec / 1000L) + (uint64_t)tv_now.tv_sec * 1000000L;
}
#endif
static uint64_t get_ticks_usec() {
#if defined(_MSC_VER)

	uint64_t ticks;

	// This is the number of clock ticks since start
	if (!QueryPerformanceCounter((LARGE_INTEGER *)&ticks))
		ticks = (UINT64)timeGetTime();

	// Divide by frequency to get the time in seconds
	// original calculation shown below is subject to overflow
	// with high ticks_per_second and a number of days since the last reboot.
	// time = ticks * 1000000L / ticks_per_second;

	// we can prevent this by either using 128 bit math
	// or separating into a calculation for seconds, and the fraction
	uint64_t seconds = ticks / ticks_per_second;

	// compiler will optimize these two into one divide
	uint64_t leftover = ticks % ticks_per_second;

	// remainder
	uint64_t time = (leftover * 1000000L) / ticks_per_second;

	// seconds
	time += seconds * 1000000L;

	// Subtract the time at game start to get
	// the time since the game started
	time -= ticks_start;
	return time;
#else
	#if defined(__APPLE__)
	uint64_t longtime = mach_absolute_time() * _clock_scale;
	#else
	struct timespec tv_now = { 0, 0 };
	clock_gettime(GODOT_CLOCK, &tv_now);
	uint64_t longtime = ((uint64_t)tv_now.tv_nsec / 1000L) + (uint64_t)tv_now.tv_sec * 1000000L;
	#endif
	longtime -= _clock_start;

	return longtime;
#endif
}

static uint64_t get_ticks_msec() {
	return get_ticks_usec() / 1000L;
}

/**
 * Establish methods we expose to GDScript and the Godot UI.
 */
void FFMpegStreamPlayer::_bind_methods() {
	// source property
	ClassDB::bind_method(D_METHOD("get_source"), &FFMpegStreamPlayer::get_source);
	ClassDB::bind_method(D_METHOD("set_source", "path"), &FFMpegStreamPlayer::set_source);
	ClassDB::add_property("FFMpegStreamPlayer", PropertyInfo(Variant::STRING, "source"), "set_source", "get_source");

	// get_loops
	// set_loops
	// loops
	ClassDB::bind_method(D_METHOD("get_loops"), &FFMpegStreamPlayer::get_loops);
	ClassDB::bind_method(D_METHOD("set_loops", "value"), &FFMpegStreamPlayer::set_loops);
	ClassDB::add_property("FFMpegStreamPlayer", PropertyInfo(Variant::BOOL, "loops"), "set_loops", "get_loops");

	// get_stream_position
	// set_stream_position
	// stream_position
	ClassDB::bind_method(D_METHOD("get_stream_position"), &FFMpegStreamPlayer::get_stream_position);
	ClassDB::bind_method(D_METHOD("set_stream_position", "path"), &FFMpegStreamPlayer::set_stream_position);
	ClassDB::add_property("FFMpegStreamPlayer", PropertyInfo(Variant::FLOAT, "position"), "set_stream_position", "get_stream_position");

	// get_duration
	ClassDB::bind_method(D_METHOD("get_duration"), &FFMpegStreamPlayer::get_duration);

	// get_frames
	ClassDB::bind_method(D_METHOD("get_frames"), &FFMpegStreamPlayer::get_frames);

	// get_mix_rate
	ClassDB::bind_method(D_METHOD("get_mix_rate"), &FFMpegStreamPlayer::get_mix_rate);

	// get_channels
	ClassDB::bind_method(D_METHOD("get_channels"), &FFMpegStreamPlayer::get_channels);

	// get_stream_position
	ClassDB::bind_method(D_METHOD("get_stream_position"), &FFMpegStreamPlayer::get_stream_position);

	// get_videoframe
	ClassDB::bind_method(D_METHOD("get_videoframe"), &FFMpegStreamPlayer::get_videoframe);

	// get_size
	ClassDB::bind_method(D_METHOD("get_size"), &FFMpegStreamPlayer::get_size);

	// get_texture
	ClassDB::bind_method(D_METHOD("get_video_texture"), &FFMpegStreamPlayer::get_video_texture);
}

void FFMpegStreamPlayer::set_source(String path) {
	if (this->_source != path) {
		this->_source = path;
		this->_prepare_decoder();
	}
}

String FFMpegStreamPlayer::get_source() const {
	return this->_source;
}

/**
 * Construct and initialize the player.
 */
FFMpegStreamPlayer::FFMpegStreamPlayer() {
	this->_unprepare_decoder();
}

/**
 * Clean up.
 */
FFMpegStreamPlayer::~FFMpegStreamPlayer() {
	// Destruct our decoder contexts
	this->_unprepare_decoder();

	// Stop our thread
	if (this->_worker.joinable()) {
		this->_playing = false;
		this->_worker.join();
	}
}

/**
 * Process the next frame.
 */
void FFMpegStreamPlayer::_process(float delta) {
	if (!this->_playing) {
		return;
	}

	//UtilityFunctions::print("FFMpegStreamPlayer::_process");

	// Keep track of our current concept of the position and retain the
	// delta in the `diff_tolerance` property.
	this->_time += delta;
	this->_diff_tolerance = delta;

	// Keep track of the audio time, as well (if there is audio)
	if (!isnan(this->_audio_time)) {
		this->_audio_time += delta;
	}
}

bool FFMpegStreamPlayer::_unprepare_decoder() {
	// Stop
	this->_playing = false;

	// Remove data packet queues
	this->_audio_packet_queue = std::queue<AVPacket>();
	this->_video_packet_queue = std::queue<AVPacket>();

	// Free SWScale Context
	if (this->_sws_ctx != NULL) {
		sws_freeContext(this->_sws_ctx);
		this->_sws_ctx = NULL;
	}

	if (this->_audio_frame != NULL) {
		av_frame_unref(this->_audio_frame);
		this->_audio_frame = NULL;
	}

	if (this->_frame_rgb != NULL) {
		av_frame_unref(this->_frame_rgb);
		this->_frame_rgb = NULL;
	}

	if (this->_frame_yuv != NULL) {
		av_frame_unref(this->_frame_yuv);
		this->_frame_yuv = NULL;
	}

	if (this->_frame_buffer != NULL) {
		Memory::free_static((void*)this->_frame_buffer);
		this->_frame_buffer = NULL;
		this->_frame_buffer_size = 0;
	}

	// Free the Video Codec Context
	if (this->_vcodec_ctx != NULL) {
		if (this->_vcodec_open) {
			avcodec_close(this->_vcodec_ctx);
		}
		avcodec_free_context(&this->_vcodec_ctx);
		this->_vcodec_ctx = NULL;
	}
	this->_vcodec_open = false;

	// Free the Audio Codec Context
	if (this->_acodec_ctx != NULL) {
		if (this->_acodec_open) {
			avcodec_close(this->_acodec_ctx);
		}
		avcodec_free_context(&this->_acodec_ctx);
		this->_acodec_ctx = NULL;
	}
	this->_acodec_open = false;

	// Free the Format Context (and close the input)
	if (this->_format_ctx != NULL) {
		if (this->_input_open) {
			UtilityFunctions::print("FFMpegStreamPlayer::_unprepare_decoder: closing input");
			avformat_close_input(&this->_format_ctx);
		}
		avformat_free_context(this->_format_ctx);
		this->_format_ctx = NULL;
	}
	this->_input_open = false;

	// Free the IO Context
	if (this->_io_ctx != NULL) {
		avio_context_free(&this->_io_ctx);
		this->_io_ctx = NULL;
	}

	// Close the file
	if (this->_file.is_open()) {
		this->_file.close();
	}

	// Deallocate the file buffer
	if (this->_io_buffer != NULL) {
		Memory::free_static((void*)this->_io_buffer);
		this->_io_buffer = NULL;
	}

	// Deallocate the audio buffer
	if (this->_audio_buffer != NULL) {
		Memory::free_static((void*)this->_audio_buffer);
		this->_audio_buffer = NULL;
	}

	// Free SWResample Context
	if (this->_swr_ctx != NULL) {
		swr_free(&this->_swr_ctx);
		this->_swr_ctx = NULL;
	}

	// Reset counters and cached values
	{
		std::lock_guard<std::mutex> guard(this->_time_mutex);
		this->_time = 0.0;
	}
	this->_seek_time = 0.0;
	this->_next_ts = -1.0;
	this->_audio_time = 0.0;
	this->_diff_tolerance = 0.0;
	this->_videostream_idx = -1;
	this->_audiostream_idx = -1;
	this->_num_decoded_samples = 0;
	this->_audio_buffer_pos = 0;
	this->_drop_frame = 0;
	this->_total_frame = 0;
	this->_frames = -1;
	this->_start_pts = -1;
	this->_duration = -1.0;
	this->_stopped_eof = false;

	// Success.
	return true;
}

// Static function passed to FFMpeg.
int FFMpegStreamPlayer::_file_read(void* opaque, uint8_t* buf, int buf_size) {
	FFMpegStreamPlayer* self = (FFMpegStreamPlayer*)opaque;
	self->_file.read((char*)buf, buf_size);

	int bytes_read = self->_file.gcount();
	if (bytes_read <= 0) {
		// Error reading
		return 0;
	}

	// Get the number of bytes read (it may be less than the buffer size)
	return bytes_read;
}

// Static function passed to FFMpeg.
int64_t FFMpegStreamPlayer::_file_seek(void* opaque, int64_t offset, int whence) {
	FFMpegStreamPlayer* self = (FFMpegStreamPlayer*)opaque;
	int pos = self->_file.tellg();
	if (whence == SEEK_SET) {
		// Reset EOF
		self->_file.clear();
		self->_file.seekg(offset, std::ios::beg);

		pos = self->_file.tellg();
	}
	else if (whence == SEEK_CUR) {
		self->_file.seekg(offset, std::ios::cur);

		pos = self->_file.tellg();
	}
	else if (whence == SEEK_END) {
		// Reset EOF
		self->_file.clear();
		self->_file.seekg(offset, std::ios::end);

		pos = self->_file.tellg();
	}
	else if (whence == AVSEEK_SIZE) {
		// Return file size
		return self->_file_size;
	}
	else {
		pos = self->_file.tellg();
	}
	return self->_file.tellg();
}

AVFormatContext* FFMpegStreamPlayer::_prepare_format() {
	// Read a chunk of data
	std::string path = this->_source.utf8().get_data();
	std::string filename = path.substr(path.find_last_of("/\\") + 1);
	UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: opening ", filename.c_str());

	this->_file.open(path, std::ios::binary);
	if (!this->_file.is_open()) {
		// Error (File can't be opened)
		//UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: file cannot be opened");
		return NULL;
	}

	// Get file size and go back to beginning
	this->_file.seekg(0, std::ios::end);
	this->_file_size = this->_file.tellg();
	this->_file.clear();
	this->_file.seekg(0, std::ios::beg);

	// Allocate and then read a chunk of the file
	this->_io_buffer = (unsigned char*)Memory::alloc_static(IO_BUFFER_SIZE);
	if (!this->_io_buffer) {
		// Error (Memory cannot be allocated)
		//UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: memory cannot be allocated");
		return NULL;
	}

	// Rewind to zero and read
	this->_file.seekg(0, std::ios::beg);
	this->_file.read((char*)this->_io_buffer, IO_BUFFER_SIZE);

	// Get the number of bytes read (it may be less than the buffer size)
	int bytes_read = this->_file.gcount();

	if (bytes_read <= 0) {
		// Error reading
		//UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot read from file");
		return NULL;
	}
	//UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: successfully read ", bytes_read, " bytes");

	// Rewind to zero again
	this->_file.clear();
	this->_file.seekg(0, std::ios::beg);
	int pos = this->_file.tellg();
	//UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: pos: ", pos);


	// Determine input format
	AVProbeData probe_data;
	probe_data.buf = this->_io_buffer;
	probe_data.buf_size = bytes_read;
	probe_data.filename = filename.c_str();
	probe_data.mime_type = "";

	const AVInputFormat* input_format = av_probe_input_format(&probe_data, 1);
	if (input_format == NULL) {
		// Error (Cannot recognize format)
		//UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot recognize format");
		return NULL;
	}

	// Create a decoder context
	this->_io_ctx = avio_alloc_context(this->_io_buffer, IO_BUFFER_SIZE, 0, this,
			this->_file_read, NULL,
			this->_file_seek);

	if (this->_io_ctx == NULL) {
		// Error (Cannot allocate IO context)
		//UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot allocate IO context");
		return NULL;
	}

	AVFormatContext* format_ctx = avformat_alloc_context();
	if (format_ctx == NULL) {
		// Error (Cannot allocate format context)
		//UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot allocate format context");
		return NULL;
	}

	format_ctx->pb = this->_io_ctx;
	format_ctx->flags = AVFMT_FLAG_CUSTOM_IO | AVFMT_SEEK_TO_PTS;
	format_ctx->iformat = input_format;

	// Success
	return format_ctx;
}

/**
 * Prepare the source file and load our decoder metadata.
 */
bool FFMpegStreamPlayer::_prepare_decoder() {
	if (this->_preparing) {
		return false;
	}
	this->_preparing = true;

	UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder");

	// Clean up anything left over, if necessary.
	this->_unprepare_decoder();

	// Set whether or not this is a "realtime" source
	this->_realtime = this->_is_real_time();
	UtilityFunctions::print("realtime? ", this->_realtime);

	// Prepare the format detection and source
	this->_format_ctx = this->_prepare_format();
	if (this->_format_ctx == NULL) {
		// Error (Cannot determine format)
		//UtilityFunctions::print("FFMpegStreamPlayer::_format_ctx: error: cannot determine format: ", av_err2str(err));
		this->_unprepare_decoder();
		return false;
	}

	AVDictionary* format_opts = NULL;
	this->_set_options(&format_opts);

	// Open the input
	std::string source = this->get_source().utf8().get_data();
	UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: opening source: ", source.c_str());
	int err = avformat_open_input(&this->_format_ctx, source.c_str(), this->_format_ctx->iformat, &format_opts);
	av_dict_free(&format_opts);

	if (err != 0) {
		// Error (Cannot open input stream)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot open input stream: ", av_err2str(err));
		this->_unprepare_decoder();
		return false;
	}
	this->_input_open = true;

	if (avformat_find_stream_info(this->_format_ctx, NULL) < 0) {
		// Error (Could not find stream info)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot find stream info");
		this->_unprepare_decoder();
		return false;
	}

	this->_videostream_idx = -1;
	this->_audiostream_idx = -1;

	// Find the stream
	for (int i = 0; i < this->_format_ctx->nb_streams; i++) {
		if (this->_format_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
			this->_videostream_idx = i;
		} else if (this->_format_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
			this->_audiostream_idx = i;
		}
	}

	if (this->_videostream_idx == -1) {
		// Error (Could not find video stream)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot find video stream");
		this->_unprepare_decoder();
		return false;
	}

	AVCodecParameters* vcodec_param = this->_format_ctx->streams[this->_videostream_idx]->codecpar;
	const AVCodec* vcodec = avcodec_find_decoder(vcodec_param->codec_id);

	if (vcodec == NULL) {
		// Error (Could not find video decoder)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot find video decoder");
		this->_unprepare_decoder();
		return false;
	}

	this->_vcodec_ctx = avcodec_alloc_context3(vcodec);
	if (this->_vcodec_ctx == NULL) {
		// Error (Could not allocate video codec)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot allocate video codec");
		this->_unprepare_decoder();
		return false;
	}

	if (avcodec_parameters_to_context(this->_vcodec_ctx, vcodec_param) < 0) {
		// Error (Could not initialize the video codec context)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot initialize video codec context");
		this->_unprepare_decoder();
		return false;
	}

	// Enable multi-thread decoding based on CPU core count.
	this->_vcodec_ctx->thread_count = 0;

	// Open the video codec
	if (avcodec_open2(this->_vcodec_ctx, vcodec, NULL) < 0) {
		// Error (Could not open video codec)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot open video codec");
		this->_unprepare_decoder();
		return false;
	}
	this->_vcodec_open = true;

	AVCodecParameters* acodec_param = NULL;
	const AVCodec* acodec = NULL;

	if (this->_audiostream_idx >= 0) {
		acodec_param = this->_format_ctx->streams[this->_audiostream_idx]->codecpar;

		acodec = avcodec_find_decoder(acodec_param->codec_id);
		if (acodec == NULL) {
			// Error (Could not find audio decoder)
			UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot find audio decoder");
			this->_unprepare_decoder();
			return false;
		}
		this->_acodec_ctx = avcodec_alloc_context3(acodec);
		if (this->_acodec_ctx == NULL) {
			// Error (Could not allocate audio codec)
			UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot allocate audio codec");
			this->_unprepare_decoder();
			return false;
		}

		if (avcodec_parameters_to_context(this->_acodec_ctx, acodec_param) < 0) {
			// Error (Could not initialize audio context)
			UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot initialize audio codec context");
			this->_unprepare_decoder();
			return false;
		}

		if (avcodec_open2(this->_acodec_ctx, acodec, NULL) < 0) {
			// Error (Could not open audio codec)
			UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot open audio codec");
			this->_unprepare_decoder();
			return false;
		}
		this->_acodec_open = true;

		this->_audio_buffer = (float*)Memory::alloc_static(AUDIO_BUFFER_MAX_SIZE * sizeof(float));
		if (this->_audio_buffer == NULL) {
			// Error (Could not allocate audio buffer)
			UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot allocate audio buffer");
			this->_unprepare_decoder();
			return false;
		}

		this->_audio_frame = av_frame_alloc();
		if (this->_audio_frame == NULL) {
			// Error (Could not allocate audio frame)
			UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot allocate audio frame");
			this->_unprepare_decoder();
			return false;
		}

		// Resample context
		this->_swr_ctx = swr_alloc();
		av_opt_set_int(this->_swr_ctx, "in_channel_layout", this->_acodec_ctx->ch_layout.u.mask, 0);
		av_opt_set_int(this->_swr_ctx, "out_channel_layout", this->_acodec_ctx->ch_layout.u.mask, 0);
		av_opt_set_int(this->_swr_ctx, "in_sample_rate", this->_acodec_ctx->sample_rate, 0);
		av_opt_set_int(this->_swr_ctx, "out_sample_rate", AUDIO_MIX_RATE, 0);
		av_opt_set_sample_fmt(this->_swr_ctx, "in_sample_fmt", AV_SAMPLE_FMT_FLTP, 0);
		av_opt_set_sample_fmt(this->_swr_ctx, "out_sample_fmt", AV_SAMPLE_FMT_FLT, 0);
		swr_init(this->_swr_ctx);
	}

	// NOTE: Align of 1 (I think it is for 32 bit alignment.) Doesn't work otherwise
	this->_frame_buffer_size = av_image_get_buffer_size(
		AV_PIX_FMT_RGB32,
		this->_vcodec_ctx->width,
		this->_vcodec_ctx->height,
		1
	);

	this->_frame_buffer = (uint8_t*)Memory::alloc_static(this->_frame_buffer_size);
	if (this->_frame_buffer == NULL) {
		// Error (Could not allocate the frame buffer)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot allocate frame buffer");
		this->_unprepare_decoder();
		return false;
	}

	this->_frame_rgb = av_frame_alloc();
	if (this->_frame_rgb == NULL) {
		// Error (Could not allocate the frame rgb buffer)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot allocate frame rgb buffer");
		this->_unprepare_decoder();
		return false;
	}

	this->_frame_yuv = av_frame_alloc();
	if (this->_frame_yuv == NULL) {
		// Error (Could not allocate the frame yuv buffer)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot allocate frame yuv buffer");
		this->_unprepare_decoder();
		return false;
	}

	int width = this->_vcodec_ctx->width;
	int height = this->_vcodec_ctx->height;
	if (av_image_fill_arrays(this->_frame_rgb->data, this->_frame_rgb->linesize, this->_frame_buffer,
				AV_PIX_FMT_RGB32, width, height, 1) < 0) {
		// Error (Could not fill the frame)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot fill frame");
		this->_unprepare_decoder();
		return false;
	}

	// Scaler context
	this->_sws_ctx = sws_getContext(
		width,
		height,
		this->_vcodec_ctx->pix_fmt,
		width,
		height,
		AV_PIX_FMT_RGB0,
		SWS_BILINEAR,
		NULL,
		NULL,
		NULL
	);
	if (this->_sws_ctx == NULL) {
		// Error (Could not create the Swscale context)
		UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: error: cannot create scaler context");
		this->_unprepare_decoder();
		return false;
	}

	// Set, if known the fps num/den
	this->_fps_num = 1;
	this->_fps_den = 1;

	AVStream* stream = this->_format_ctx->streams[this->_videostream_idx];
	this->_fps_num = stream->r_frame_rate.num;
	this->_fps_den = stream->r_frame_rate.den;

	// Reconstruct the texture
	this->_texture = Ref<ImageTexture>(memnew(ImageTexture));
	Vector2 size = this->get_size();
	Ref<Image> img = Image::create(size.x, size.y, false, Image::FORMAT_RGBA8);
	this->_texture->set_image(img);
	this->set_texture(this->_texture);

	// Done.
	UtilityFunctions::print("FFMpegStreamPlayer::_prepare_decoder: now playing");
	this->_preparing = false;

	// Start/Restart our consumer thread
	if (this->_worker.joinable()) {
		this->_playing = false;
		this->_worker.join();
	}

	this->_playing = true;
	this->_worker = std::thread(&FFMpegStreamPlayer::_loop, this);

	return true;
}

int64_t FFMpegStreamPlayer::_get_stream_start_position() const {
	// Return any retained value.
	if (this->_start_pts >= 0) {
		return this->_start_pts;
	}

	// Read from stream. If the value read isn't valid, get it from the first frame in the stream that provides such a
	// value.
	AVStream* stream = this->_format_ctx->streams[this->_videostream_idx];
	int64_t startPTS = stream->start_time;
	AVPacket* pkt = av_packet_alloc();
	av_packet_unref(pkt);

	if (startPTS == int64_t(AV_NOPTS_VALUE)) {
		//UtilityFunctions::print("FFMpegStreamPlayer::_get_stream_start_position: start_time == AV_NOPTS_VALUE");
		// Seek 1st key-frame in video stream.
		avcodec_flush_buffers(this->_vcodec_ctx);

		//UtilityFunctions::print("FFMpegStreamPlayer::_get_stream_start_position: seeking first key frame");
		if (av_seek_frame(this->_format_ctx, this->_videostream_idx, 0, 0) >= 0) {
			av_packet_unref(pkt);

			// Read frames until we get one for the video stream that contains a valid PTS.
			//UtilityFunctions::print("FFMpegStreamPlayer::_get_stream_start_position: seeking PTS frame");
			do {
				if (av_read_frame(this->_format_ctx, pkt) < 0)  {
					//UtilityFunctions::print("FFMpegStreamPlayer::_get_stream_start_position: error reading frame. bailing.");
					// Read error or EOF. Abort search for PTS.
					break;
				}

				if (pkt->stream_index == this->_videostream_idx) {
					// Packet read for video stream. Get its PTS. Loop will continue if the PTS is AV_NOPTS_VALUE.
					//UtilityFunctions::print("FFMpegStreamPlayer::_get_stream_start_position: found PTS: ", pkt.pts);
					startPTS = pkt->pts;
				}

				av_packet_unref(pkt);
			} while (startPTS == int64_t(AV_NOPTS_VALUE));
		}
	}

	// If we still don't have a valid initial PTS, assume 0. (This really shouldn't happen for any real media file, as
	// it would make meaningful playback presentation timing and seeking impossible.)
	if (startPTS == int64_t(AV_NOPTS_VALUE)) {
		//UtilityFunctions::print("FFMpegStreamPlayer::_get_stream_start_position: start_time cannot be determined... returning 0");
		startPTS = 0;
	}

	//UtilityFunctions::print("FFMpegStreamPlayer::_get_stream_start_position: _start_pts: ", startPTS);
	this->_start_pts = startPTS;
	return this->_start_pts;
}


int64_t FFMpegStreamPlayer::_frame_to_pts(int frame) const {
	// Get the starting point for the stream (_start_pts)
	this->_get_stream_start_position();

	AVStream* stream = this->_format_ctx->streams[this->_videostream_idx];
	return this->_start_pts + (int64_t(frame) * this->_fps_den * stream->time_base.den) / 
		(int64_t(_fps_num) * stream->time_base.num);
}

int FFMpegStreamPlayer::_pts_to_frame(int64_t pts) const {
	// Get the starting point for the stream (_start_pts)
	this->_get_stream_start_position();

	AVStream* stream = this->_format_ctx->streams[this->_videostream_idx];
	return (int64_t(pts - this->_start_pts) * stream->time_base.num * this->_fps_num) / 
		(int64_t(stream->time_base.den) * this->_fps_den);
}

bool FFMpegStreamPlayer::get_loops() const {
	return this->_loops;
}

void FFMpegStreamPlayer::set_loops(bool value) {
	this->_loops = value;
	UtilityFunctions::print("FFMpegStreamPlayer::set_loops: to ", value);

	// Start playing again if we are at the eof
	if (!this->_playing && this->_stopped_eof) {
		UtilityFunctions::print("FFMpegStreamPlayer::set_loops: restarting...");
		this->_stopped_eof = false;
		this->set_stream_position(0);
		this->play();
	}
}

int FFMpegStreamPlayer::get_frames() const {
	if (this->_frames >= 0) {
		return this->_frames;
	}

	// Get a reference to the video stream.
	AVStream* stream = this->_format_ctx->streams[this->_videostream_idx];

	// It may just contain metadata for the number of frames
	if (stream->nb_frames > 0) {
		//UtilityFunctions::print("FFMpegStreamPlayer::get_frames: nb_frames: ", stream->nb_frames);
		this->_frames = stream->nb_frames;
		return this->_frames;
	}

	// We must calculate the duration by seeking frames.
	// Get the starting frame index
	int64_t maxPts = this->_get_stream_start_position();

	// Seek last key-frame.
	//UtilityFunctions::print("FFMpegStreamPlayer::get_frames: seeking last key frame.");
	avcodec_flush_buffers(this->_vcodec_ctx);
	av_seek_frame(this->_format_ctx, this->_videostream_idx, this->_frame_to_pts(1<<29), AVSEEK_FLAG_BACKWARD);

	// Read up to last frame, extending max PTS for every valid PTS value found for the video stream.
	AVPacket pkt;

	int pos = this->_file.tellg();
	//UtilityFunctions::print("FFMpegStreamPlayer::get_frames: reading frames until we can't. tellg: ", pos);
	av_packet_unref(&pkt);
	int ret = av_read_frame(this->_format_ctx, &pkt);
	//UtilityFunctions::print("FFMpegStreamPlayer::get_frames: ret: ", ret);
	while (ret >= 0) {
		if (pkt.stream_index == this->_videostream_idx) {
			if (pkt.pts != int64_t(AV_NOPTS_VALUE) && pkt.pts > maxPts) {
				//UtilityFunctions::print("FFMpegStreamPlayer::get_frames: found new PTS value: ", pkt->pts);
				maxPts = pkt.pts;
			}
		}
		av_packet_unref(&pkt);
		ret = av_read_frame(this->_format_ctx, &pkt);
		//UtilityFunctions::print("FFMpegStreamPlayer::get_frames: ret: ", ret);
	}

	if (ret < 0) {
		//UtilityFunctions::print("FFMpegStreamPlayer::get_frames: av_read_frame error: ", av_err2str(ret));
	}

	// Compute frame range from min to max PTS. Need to add 1 as both min and max are at starts of frames, so stream
	// extends for 1 frame beyond this.

	//UtilityFunctions::print("FFMpegStreamPlayer::get_frames: startPts: ", this->_start_pts);
	//UtilityFunctions::print("FFMpegStreamPlayer::get_frames: maxPts: ", maxPts);
	int frames = 1 + this->_pts_to_frame(maxPts);
	this->_frames = frames;
	return this->_frames;
}

/**
 * Get the duration of the video in seconds.
 */
float FFMpegStreamPlayer::get_duration() const {
	// We must have an open format context to query duration.
	if (this->_format_ctx == NULL) {
		// Error (No format context)
		return -1;
	}

	// Return the cached result.
	if (this->_duration >= 0.0) {
		return this->_duration;
	}

	// Get a reference to the video stream.
	AVStream* stream = this->_format_ctx->streams[this->_videostream_idx];

	// If the duration is simply known, we return it.
	if (stream->duration != AV_NOPTS_VALUE) {
		//UtilityFunctions::print("FFMpegStreamPlayer::get_duration: ", this->_format_ctx->streams[this->_videostream_idx]->duration, " * ", av_q2d(this->_format_ctx->streams[this->_videostream_idx]->time_base), ";");
		// We convert from the internal unit FFMpeg uses to seconds.
		this->_duration = ((double)stream->duration) * av_q2d(stream->time_base);
		return this->_duration;
	}
	//UtilityFunctions::print("FFMpegStreamPlayer::get_duration: duration == AV_NOPTS_VALUE");

	// Maybe we can figure it out by looking at the number of frames.
	int64_t nb_frames = this->get_frames();
	if (nb_frames >= 0) {
		double frame_rate = stream->r_frame_rate.num / (double)stream->r_frame_rate.den;
		//UtilityFunctions::print("FFMpegStreamPlayer::get_duration: nb_frames: ", nb_frames);
		//UtilityFunctions::print("FFMpegStreamPlayer::get_duration: frame_rate: ", frame_rate);
		this->_duration = ((double)nb_frames) / frame_rate;
		return this->_duration;
	}

	//UtilityFunctions::print("FFMpegStreamPlayer::get_duration: nb_frames <= 0");
	return 0.0;
}

float FFMpegStreamPlayer::get_framerate() const {
	AVStream* stream = this->_format_ctx->streams[this->_videostream_idx];
	return stream->r_frame_rate.num / (double)stream->r_frame_rate.den;
}

/**
 * Get the position of the video.
 */
float FFMpegStreamPlayer::get_stream_position() const {
	return 0.0;
}

/**
 * Set the position of the video.
 */
void FFMpegStreamPlayer::set_stream_position(float time) {
	// Hack to find the end of the video. Really VideoPlayer should expose this!
	if (time < 0) {
		time = FFMpegStreamPlayer::_avtime_to_sec(this->get_duration());
	}

	// Where we want to end up (in ffmpeg units)
	int64_t seek_target = time * AV_TIME_BASE;

	// Seek within 10 seconds of the selected spot.
	int64_t margin = 10 * AV_TIME_BASE;

	// Perform the seek
	int ret = avformat_seek_file(this->_format_ctx, -1, seek_target - margin, seek_target, seek_target, 0);
	if (ret < 0) {
		// Error (Cannot seek backwards, perhaps... we will try a little further ahead)
		ret = avformat_seek_file(this->_format_ctx, -1, seek_target - margin, seek_target, seek_target + margin, 0);
	}

	if (ret < 0) {
		// Error (Cannot seek)
		//UtilityFunctions::print("FFMpegStreamPlayer::set_stream_position: error: cannot seek");
	} else {
		//UtilityFunctions::print("FFMpegStreamPlayer::set_stream_position: queue empty");
		this->_audio_packet_queue = std::queue<AVPacket>();
		this->_video_packet_queue = std::queue<AVPacket>();
		FFMpegStreamPlayer::_flush_frames(this->_vcodec_ctx);
		avcodec_flush_buffers(this->_vcodec_ctx);
		if (this->_acodec_ctx) {
			FFMpegStreamPlayer::_flush_frames(this->_acodec_ctx);
			avcodec_flush_buffers(this->_acodec_ctx);
		}
		this->_num_decoded_samples = 0;
		this->_audio_buffer_pos = 0;
		{
			std::lock_guard<std::mutex> guard(this->_time_mutex);
			this->_time = time;
		}
		this->_seek_time = time;
		this->_next_ts = -1.0;

		// Try to use the audio time as the seek position
		this->_position_type = POS_A_TIME;
		this->_audio_time = NAN;

		// Throw out the current frame
		this->_frame_unwrapped = false;

		// Forget if we stopped due to rendering the last frame
		this->_stopped_eof = false;
	}
}

/**
 * Writes the given frame to our unwrapped frame buffer byte array.
 */
void FFMpegStreamPlayer::_unwrap_video_frame(AVFrame* frame, int width, int height) {
	//UtilityFunctions::print("FFMpegStreamPlayer::_unwrap_video_frame");
	int frame_size = width * height * 4;
	if (this->_frame_data.size() != frame_size) {
		this->_frame_data.resize(frame_size);
	}

	uint8_t* write_ptr = this->_frame_data.ptrw();
	int val = 0;
	for (int y = 0; y < height; y++) {
		memcpy(write_ptr, frame->data[0] + y * frame->linesize[0], width * 4);
		write_ptr += width * 4;
	}

	// Create an image (zero-copy)
	//UtilityFunctions::print("FFMpegStreamPlayer::_unwrap_video_frame: new Image (", width, " x ", height, ")");
	Ref<Image> img = Image::create_from_data(width, height, 0, Image::FORMAT_RGBA8, this->_frame_data);

	// Zero copy transfer
	//UtilityFunctions::print("FFMpegStreamPlayer::_unwrap_video_frame: update texture");
	this->_texture->call_deferred("update", img);

	//UtilityFunctions::print("FFMpegStreamPlayer::_unwrap_video_frame: updated texture: ", this->_texture->get_width(), " x ", this->_texture->get_height());
}

/**
 * Reads the next frame.
 */
int FFMpegStreamPlayer::_read_frame() {
	// Bail if we already meet the threshold
	if (!this->_realtime && this->_video_packet_queue.size() >= VIDEO_BUFFER_MAX) {
		return 0;
	}

	// We try to get enough packets
	while (this->_video_packet_queue.size() < (this->_realtime ? 1 : VIDEO_BUFFER_MAX)) {
		AVPacket pkt;
		int ret = av_read_frame(this->_format_ctx, &pkt);
		if (ret >= 0) {
			if (pkt.stream_index == this->_videostream_idx) {
				this->_video_packet_queue.push(pkt);
				//UtilityFunctions::print("FFMpegStreamPlayer::_read_frame: push packet size: ", this->_video_packet_queue.size());
			} else if (pkt.stream_index == this->_audiostream_idx) {
				this->_audio_packet_queue.push(pkt);
			} else {
				av_packet_unref(&pkt);
			}
		}
		else if (ret == AVERROR_EOF) {
			// End of file
			UtilityFunctions::print("FFMpegStreamPlayer::_read_frame: av_read_frame: error: ", ret, ": ", av_err2str(ret));
			return AVERROR_EOF;
		}
		else {
			UtilityFunctions::print("FFMpegStreamPlayer::_read_frame: av_read_frame: error: ", ret, ": ", av_err2str(ret));
			return -1;
		}
	}

	return 0;
}

/**
 * Feeds the current video frame.
 */
float FFMpegStreamPlayer::get_videoframe(float time) {
	static float _stalled = -1.0;
	// Wait for the next timestamp... return the last frame if we
	// aren't ready for a new one yet.
	if (!this->_realtime && this->_next_ts >= 0.0 && time < this->_next_ts) {
		if (_stalled < 0.0) {
			_stalled = get_ticks_msec() - 0.51;
		}
		if (get_ticks_msec() - _stalled > 0.5) {
			_stalled = get_ticks_msec();
			//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: stalled.");
		}
		return time;
	}
	_stalled = -1.0;

	AVPacket pkt = {0};
	int ret;
	size_t drop_count = 0;
	// to maintain a decent game frame rate
	// don't let frame decoding take more than this number of ms
	uint64_t max_frame_drop_time = 5;

	// but we do need to drop frames, so try to drop at least some frames even if it's a bit slow :(
	size_t min_frame_drop_count = 1;
	uint64_t start = get_ticks_msec();

retry:
	//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: avcodec_receive_frame starting...");
	ret = avcodec_receive_frame(this->_vcodec_ctx, this->_frame_yuv);
	//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: avcodec_receive_frame ", ret);
	if (ret == AVERROR(EAGAIN)) {
		// need to call avcodec_send_packet, get a packet from queue to send it
		while (this->_video_packet_queue.size() == 0) {
			//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: video packet queue empty");

			ret = this->_read_frame();
			if (ret == AVERROR_EOF) {
				UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: eof");

				if (this->_eof) {
					// We flushed our own queue.
					// Push NULL packet stream to go into drain mode.
					avcodec_send_packet(this->_vcodec_ctx, NULL);
				}

				this->_eof = true;

				// Stop
				//this->_playing = false;
				goto retry;
			}

			if (this->_video_packet_queue.size() == 0) {
				UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: cannot read frame");
				return time;
			}
		}
		pkt = this->_video_packet_queue.front();
		this->_video_packet_queue.pop();
		//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: send packet: new size: ", this->_video_packet_queue.size());

		ret = avcodec_send_packet(this->_vcodec_ctx, &pkt);
		if (ret < 0) {
			UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: avcodec_send_packet returns ", ret, ": ", av_err2str(ret));
			av_packet_unref(&pkt);
			return time;
		}
		av_packet_unref(&pkt);

		// TODO: get rid of this goto
		goto retry;
	}
	else if (ret < 0) {
		//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: avcodec_receive_frame returns ", ret, ": ", av_err2str(ret));

		// This is fine... as long as we have something in the queue
		if (this->_video_packet_queue.size() == 0) {
			UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: cannot read frame (eof)");

			// If we alreay know we hit the end of the file...
			// loop maybe
			if (this->_eof && this->_loops) {
				this->set_stream_position(0);
				this->_eof = false;
			}
			else if (this->_eof) {
				this->_playing = false;
				this->set_stream_position(0);
				this->_eof = false;
				this->_stopped_eof = true;
			}

			// Return the new timestamp
			return this->_time;
		}
		return time;
	}

	//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: deciding pts");

	// We get the PTS (precision timestamp) which is what the video thinks the time
	// should be. We eventually convert that into units of seconds in `ts`.
	bool pts_correct = this->_frame_yuv->pts == AV_NOPTS_VALUE;
	int64_t pts = pts_correct ? this->_frame_yuv->pkt_dts : this->_frame_yuv->pts;
	double ts = pts * av_q2d(this->_format_ctx->streams[this->_videostream_idx]->time_base);

	// Correct our timestamp to the calculated one, if we have an empty frame buffer
	if (this->_next_ts < 0.0) {
		time = ts;
		this->_seek_time = ts;
	}

	// We increment our concept of how many frames we have rendered.
	this->_total_frame++;

	//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: total_frame: ", (int)this->_total_frame);

	// Frame successfully decoded here, now if it lags behind too much (diff_tolerance sec)
	// let's discard this frame and get the next frame instead.
	bool drop = ts < (time - this->_diff_tolerance);

	//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: drop: ", drop, ", ts: ", ts, ", _time: ", time, ", _diff_tolerance: ", this->_diff_tolerance);
	uint64_t drop_duration = get_ticks_msec() - start;
	if (drop && drop_duration > max_frame_drop_time && drop_count < min_frame_drop_count && this->_frame_unwrapped) {
		//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: drop is true: drop_duration: ", drop_duration, ", max_frame_drop_time: ", max_frame_drop_time, ", drop_count: ", drop_count, ", min_frame_drop_count: ", min_frame_drop_count, ", _frame_unwrapped: ", this->_frame_unwrapped);
		// only discard frames for max_frame_drop_time ms or we'll slow down the main thread!
		if (fabs(this->_seek_time - time) > this->_diff_tolerance * 10) {
			//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: profiling: CPU slow, dropping ", drop_count, " frames (", drop_duration, ")");
		}
	}
	else if (drop) {
		// We have missed our deadline... so don't copy the data.
		// Get the next frame.
		drop_count++;
		this->_drop_frame++;
		av_packet_unref(&pkt);
		UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: drop: goto retry");
		goto retry;
	}

	if (!drop || fabs(this->_seek_time - time) > this->_diff_tolerance * 2) {
		// Don't overwrite the current frame when dropping frames for performance reasons
		// except when the time is within 2 frames of the most recent seek
		// because we don't want a glitchy 'fast forward' effect when seeking.
		// NOTE: VideoPlayer currently doesn't ask for a frame when seeking while paused so you'd
		// have to fake it inside godot by unpausing briefly. (see FIG1 below)
		//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: unwrapping.");
		this->_frame_unwrapped = true;
		sws_scale(this->_sws_ctx, (uint8_t const *const *)this->_frame_yuv->data, this->_frame_yuv->linesize, 0,
				this->_vcodec_ctx->height, this->_frame_rgb->data, this->_frame_rgb->linesize);
		_unwrap_video_frame(this->_frame_rgb, this->_vcodec_ctx->width, this->_vcodec_ctx->height);
	}
	av_packet_unref(&pkt);

	// Calculate the next timestamp we expect
	this->_next_ts = ts + (1.0 / this->get_framerate());
	if (!this->_frame_unwrapped) {
		return time;
	}

	//UtilityFunctions::print("FFMpegStreamPlayer::get_videoframe: returning texture; next_ts: ", this->_next_ts, " frate: ", this->get_framerate());
	return time;
}

void FFMpegStreamPlayer::_flush_frames(AVCodecContext* ctx) {
	/**
	 * from https://www.ffmpeg.org/doxygen/4.1/group__lavc__encdec.html
	 * End of stream situations. These require "flushing" (aka draining) the codec, as the codec might buffer multiple frames or packets internally for performance or out of necessity (consider B-frames). This is handled as follows:
	 * Instead of valid input, send NULL to the avcodec_send_packet() (decoding) or avcodec_send_frame() (encoding) functions. This will enter draining mode.
	 * Call avcodec_receive_frame() (decoding) or avcodec_receive_packet() (encoding) in a loop until AVERROR_EOF is returned. The functions will not return AVERROR(EAGAIN), unless you forgot to enter draining mode.
	 * Before decoding can be resumed again, the codec has to be reset with avcodec_flush_buffers().
	 */
	int ret = avcodec_send_packet(ctx, NULL);
	AVFrame frame = {0};
	if (ret <= 0) {
		do {
			ret = avcodec_receive_frame(ctx, &frame);
		} while (ret != AVERROR_EOF);
	}
}

double FFMpegStreamPlayer::_avtime_to_sec(int64_t avtime) {
	return avtime / (double)AV_TIME_BASE;
}

void FFMpegStreamPlayer::set_audio_track(int audiotrack) {
	// TODO: implement
}

int FFMpegStreamPlayer::get_channels() const {
	if (this->_acodec_ctx != NULL) {
		return this->_acodec_ctx->ch_layout.nb_channels;
	}

	return 0;
}

int FFMpegStreamPlayer::get_mix_rate() const {
	if (this->_acodec_ctx != NULL) {
		return AUDIO_MIX_RATE;
	}

	return 0;
}

Vector2 FFMpegStreamPlayer::get_size() const {
	return Vector2(this->_vcodec_ctx->width, this->_vcodec_ctx->height);
}

Ref<Texture2D> FFMpegStreamPlayer::get_video_texture() const {
	return this->_texture;
}

int FFMpegStreamPlayer::get_audio() {
	// Detect if playback just started or we just seeked.
	// If so, we want to enter the audio_reset state.
	bool audio_reset = this->_audio_time < 0.0 || this->_audio_time > (this->_time - this->_diff_tolerance);

	bool first_frame = true;

	float* pcm = NULL; // TODO: input
	int pcm_remaining = 0; //TODO: input

	const int pcm_buffer_size = pcm_remaining;
	int pcm_offset = 0;

	AVStream* stream = this->_format_ctx->streams[this->_audiostream_idx];
	double p_time = this->_audio_frame->pts * av_q2d(stream->time_base);

	if (audio_reset && this->_num_decoded_samples > 0) {
		// Do not send PCM data if the frame has not started yet.
		if (p_time > this->_time) {
			return 0;
		}

		// Skip any decoded samples if their timestamp is too old.
		if (this->_time - p_time > this->_diff_tolerance) {
			this->_num_decoded_samples = 0;
		}
	}

	int sample_count = (pcm_remaining < this->_num_decoded_samples)
		         ? pcm_remaining
			 : this->_num_decoded_samples;

	int channels = this->_acodec_ctx->ch_layout.nb_channels;

	if (sample_count > 0) {
		memcpy(pcm, this->_audio_buffer + (channels * this->_audio_buffer_pos), sizeof(float) * sample_count * channels);
		pcm_offset += sample_count;
		pcm_remaining -= sample_count;
		this->_num_decoded_samples -= sample_count;
		this->_audio_buffer_pos += sample_count;
	}

	while (pcm_remaining > 0) {
		if (this->_num_decoded_samples <= 0) {
			AVPacket pkt;
			int ret;
retry_audio:
			ret = avcodec_receive_frame(this->_acodec_ctx, this->_audio_frame);
			if (ret == AVERROR(EAGAIN)) {
				// Need to send packets.
				if (this->_audio_packet_queue.size() == 0) {
					if (pcm_offset == 0) {
						// If we haven't gotten any on-time audio yet,
						// then the audio_time counter is meaningless.
						this->_audio_time = -1.0;
					}
					return pcm_offset;
				}

				// Get a packet.
				pkt = this->_video_packet_queue.front();
				this->_video_packet_queue.pop();

				// Send the packet.
				ret = avcodec_send_packet(this->_acodec_ctx, &pkt);
				if (ret < 0) {
					// XXX: ERROR
					return pcm_offset;
				}

				av_packet_unref(&pkt);
				goto retry_audio;
			}
			else if (ret < 0) {
				// XXX: ERROR
				return pcm_buffer_size - pcm_remaining;
			}

			// Only set the audio frame time if this is the first
			// frame we've decoded during this update.
			//
			// Any remaining frames are going into a buffer anyways.
			p_time = this->_audio_frame->pts * av_q2d(stream->time_base);
			if (first_frame) {
				this->_audio_time = p_time;
				first_frame = false;
			}

			// Decoded audio ready here. (XXX: Could interleave here)
			this->_num_decoded_samples = swr_convert(
				this->_swr_ctx,
				(uint8_t**)&this->_audio_buffer,
				this->_audio_frame->nb_samples,
				(const uint8_t**)this->_audio_frame->extended_data,
				this->_audio_frame->nb_samples
			);

			this->_audio_buffer_pos = 0;
		}

		if (audio_reset) {
			if (this->_time - p_time > this->_diff_tolerance) {
				// Skip samples if frame time is too far in the past
				this->_num_decoded_samples = 0;
			}
			else if (p_time > this->_time) {
				// Don't send any PCM data if the first frame hasn't
				// started yet.
				this->_audio_time = -1.0;
				break;
			}
		}

		sample_count = pcm_remaining < this->_num_decoded_samples
			     ? pcm_remaining
			     : this->_num_decoded_samples;
		if (sample_count > 0) {
			memcpy(
				pcm + pcm_offset * channels,
				this->_audio_buffer + (channels * this->_audio_buffer_pos),
				sizeof(float) * sample_count * channels
			);

			pcm_offset += sample_count;
			pcm_remaining -= sample_count;
			this->_num_decoded_samples -= sample_count;
			this->_audio_buffer_pos += sample_count;
		}
	}

	return pcm_offset;
}

void FFMpegStreamPlayer::play() {
	if (!this->_playing) {
		if (this->_worker.joinable()) {
			this->_worker.join();
		}

		this->_playing = true;
		this->_worker = std::thread(&FFMpegStreamPlayer::_loop, this);
	}
}

void FFMpegStreamPlayer::pause() {
	this->_playing = false;
}

void FFMpegStreamPlayer::stop() {
	this->_playing = false;
	this->set_stream_position(0);
}

bool FFMpegStreamPlayer::_is_real_time() {
	return false;
}

/**
 * This loop continuously consumes a video frame.
 */
void FFMpegStreamPlayer::_loop() {
	UtilityFunctions::print("FFMpegStreamPlayer::_loop starting");
	
	while (this->_playing) {
		// Synchronize on the elapsed time
		float time = 0.0;
		{
			std::lock_guard<std::mutex> guard(this->_time_mutex);
			time = this->_time;
		}


		// Read the appropriate frames to maintain our buffer
		float new_time = this->get_videoframe(time);

		{
			// Re-sync, if possible
			std::lock_guard<std::mutex> guard(this->_time_mutex);
			if (this->_time == time && time != new_time) {
				this->_time = new_time;
			}
		}

		// Yield
		//using namespace std::chrono_literals;
		//std::this_thread::sleep_for(1ms);
	}
	
	UtilityFunctions::print("FFMpegStreamPlayer::_loop ending");
}

void FFMpegStreamPlayer::_set_options(AVDictionary** format_opts) const {
	av_dict_set(format_opts, "scan_all_pmts", "1", AV_DICT_DONT_OVERWRITE);
}
