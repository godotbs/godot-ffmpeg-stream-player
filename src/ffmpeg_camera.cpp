#include "ffmpeg_camera.hpp"

#include <godot_cpp/variant/utility_functions.hpp>

using namespace godot;

/**
 * Construct and initialize the class.
 */
FFMpegCamera::FFMpegCamera() {
	UtilityFunctions::print("FFMpegCamera::FFMpegCamera: registering devices");
	avdevice_register_all();

	this->_unprepare_decoder();
}

/**
 * Create resources.
 */
AVFormatContext* FFMpegCamera::_prepare_format() {
	UtilityFunctions::print("FFMpegCamera::_prepare_format(): preparing format");

	// Get the 'dshow' input format, for Windows
	const AVInputFormat* input_format = av_find_input_format("dshow");
	if (input_format == NULL) {
		// Error (Cannot allocate format context)
		UtilityFunctions::print("FFMpegCamera::_prepare_format(): failure finding dshow");
		return NULL;
	}

	// Get device list
	AVDeviceInfoList* deviceList;
	int deviceCount = avdevice_list_input_sources(input_format, nullptr, nullptr, &deviceList);
	UtilityFunctions::print("FFMpegCamera::_prepare_format: deviceCount: ", deviceCount);

	int default_devindex = Math::max(deviceList->default_device, 0);
	UtilityFunctions::print("FFMpegCamera::_prepare_format: default_devindex: ", default_devindex);

	if (deviceList->nb_devices > 0) {
		std::string source_str = "video=";
		std::string device_str = deviceList->devices[default_devindex]->device_description;
		source_str = source_str + device_str;
		this->set_source(source_str.c_str());
	}

	for (int devindex = 0; devindex < deviceList->nb_devices; devindex++) {
		AVDeviceInfo* deviceInfo = deviceList->devices[devindex];
		UtilityFunctions::print("Device name: ", deviceInfo->device_name);
		UtilityFunctions::print("Device desc: ", deviceInfo->device_description);

		// Print media types
		for (int i = 0; i < deviceInfo->nb_media_types; i++) {
			AVMediaType type = deviceInfo->media_types[i];
			switch (type) {
				case AVMEDIA_TYPE_AUDIO:
					UtilityFunctions::print("Device type: audio");
					break;

				case AVMEDIA_TYPE_DATA:
					UtilityFunctions::print("Device type: data");
					break;

				case AVMEDIA_TYPE_SUBTITLE:
					UtilityFunctions::print("Device type: subtitle");
					break;

				case AVMEDIA_TYPE_ATTACHMENT:
					UtilityFunctions::print("Device type: attachment");
					break;

				case AVMEDIA_TYPE_VIDEO:
					UtilityFunctions::print("Device type: video");
					break;

				case AVMEDIA_TYPE_UNKNOWN:
				default:
					UtilityFunctions::print("Device type: unknown");
					break;
			}
		}
	}

	// Free device list
	avdevice_free_list_devices(&deviceList);

	// Generate the format context
	AVFormatContext* format_ctx = avformat_alloc_context();

	// Set the input format (dshow driver, e.g.)
	format_ctx->iformat = input_format;

	// Try to use the MJPEG decoder
	format_ctx->video_codec_id = AV_CODEC_ID_MJPEG;

	// Success.
	return format_ctx;
}

/**
 * Clean up.
 */
FFMpegCamera::~FFMpegCamera() {
	this->_unprepare_decoder();
}

/**
 * Establish methods we expose to GDScript and the Godot UI.
 */
void FFMpegCamera::_bind_methods() {
}

bool FFMpegCamera::_is_real_time() {
	return true;
}

void FFMpegCamera::_set_options(AVDictionary** format_opts) const {
	UtilityFunctions::print("FFMpegCamera::FFMpegCamera: setting options");
	av_dict_set(format_opts, "video_size", "1920x1080", 0);
	av_dict_set(format_opts, "framerate", "30", 0);
}
