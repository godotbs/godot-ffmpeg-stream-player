#ifndef FFMPEG_CAMERA_HPP
#define FFMPEG_CAMERA_HPP

// Import standard library functions
#include <thread>
#include <chrono>

// Import Godot support classes
#include <godot_cpp/core/class_db.hpp>

// Import base class
#include <godot_cpp/classes/sprite2d.hpp>

// Import support classes
#include <godot_cpp/classes/ref.hpp>
#include <godot_cpp/templates/vector.hpp>
#include <godot_cpp/classes/image.hpp>
#include <godot_cpp/classes/image_texture.hpp>

// Import FFMpeg
#include "ffmpeg_common.h"

// Import FFMpegStreamPlayer
#include "ffmpeg_stream_player.hpp"

namespace godot {
	class FFMpegCamera : public FFMpegStreamPlayer {
		GDCLASS(FFMpegCamera, FFMpegStreamPlayer)

		private:

		public:
			FFMpegCamera();
			~FFMpegCamera();

		protected:
			AVFormatContext* _prepare_format();
			bool _is_real_time();
			void _set_options(AVDictionary** format_opts) const;
			static void _bind_methods();
	};

}

#endif
