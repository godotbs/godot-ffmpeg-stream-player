# Godot FFmpeg Stream Player

This is a GDExtension that wraps the FFmpeg project for rendering video streams
using the variety of file formats that FFmpeg allows.

## License

This project heavily uses the FFmpeg project, which is licensed under the LGPL.
This plugin is, then, also licensed under the same license: LGPL v2.1 or later.

The FFmpeg build is not configured to enable parts that are under the more
restrictive GPL license by default. See `LICENSE` for the generic LGPL license
this repository's code is under. Then, refer to the `LICENSE.md` file within
the FFmpeg source path for specific information about the licensing of FFmpeg
itself.

## Building

To build whatever works for the platform you are currently on:

```shell
scons target=template_release
```

To build a version suitable for the debugging environment of Godot:

```shell
scons target=template_debug
```

To build for a particular platform, specify `windows`, `linux`, or `macos`
to the `platform` field:

```shell
scons target=template_release platform=windows
```

To build for a particular version of Godot, specify that as an argument via
the `godot_version` parameter:

```shell
scons target=template_release godot_version=4.1
```

By default, the script will target the latest Godot release. This extension
is not necessarily compatible with all releases.
